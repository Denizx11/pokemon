import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { finalize } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon, PokemonResponse } from "../models/pokemon.model";
const {apiPokemons} = environment;

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  private _pokemons: Pokemon[] = [];
  private _pokemonResponse?: PokemonResponse;
  private _error: string = "";
  private _loading: boolean = false;

  public get pokemons(): Pokemon[] {
    return this._pokemons;
  }

  public get error(){
    return this._error;
  }

  get loading(): boolean {
    return this._loading;
  }

  constructor(private readonly http: HttpClient) { }

  public findAllPokemons(): void {

    if (this._pokemons.length > 0 || this.loading){
      return;
    }

    this._loading = true;
    this.http.get<PokemonResponse>(apiPokemons)
    .pipe(
      finalize(() => {
        this._loading = false;
      })
    )
    .subscribe({
      next: (pokemons: PokemonResponse) => {
        this._pokemonResponse = pokemons;
        this._pokemons=this._pokemonResponse.results;
      },
      error: (error: HttpErrorResponse) => {
        this._error = error.message;
      }
    })
  }

  public findPokemon(url: string): Pokemon | undefined {
    return this.pokemons.find((pokemon: Pokemon) => pokemon.url === url);
  }

  public getIdAndImage(url?: string): any {
    const id = url?.split('/').filter( Boolean ).pop();
    return { id, image: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${ id }.png`};
  }
}
