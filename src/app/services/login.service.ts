import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of, switchMap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon } from '../models/pokemon.model';
import { Trainer } from '../models/trainer.model';
import { User } from '../models/user.model';

const {apiTrainers, apiKey} = environment;

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  //dependency injection
  constructor(private readonly http: HttpClient) { }

  //Models, HttpClient, Observables, and RxJS operators

  public login(username: string): Observable<Trainer> {
    
    
    return this.checkUsername(username).pipe(
      switchMap((user: Trainer | undefined) => {
        if (user === undefined) { //if user does not exist
          return this.createUser(username)
        }

        return of(user); 
      }), 
    )
  }
  //login
  
  //check if user exists
  private checkUsername(username: string):Observable<Trainer | undefined> {

    return this.http.get<Trainer[]>(`${apiTrainers}?username=${username}`).pipe(
      //RxJS Operators
      map((response: Trainer[]) => response.pop())
    )
  }
  //create user
  private createUser (username: string): Observable<Trainer> {
    //create user
    const user = {
      username,
      pokemons: []
    };
    //headers -> api key
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-api-key": apiKey
    })
    //post - create item in 
    return this.http.post<Trainer>(apiTrainers, user, {
      headers
    })
  }

}
