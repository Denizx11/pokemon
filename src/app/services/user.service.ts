import { Injectable } from '@angular/core';
import { StorageKeys } from '../enums/storage-keys.enum';
import { Guitar } from '../models/guitar.model';
import { Pokemon } from '../models/pokemon.model';
import { Trainer } from '../models/trainer.model';
import { User } from '../models/user.model';
import { StorageUtil } from '../utils/storage.util';

@Injectable({
  providedIn: 'root'
})
export class UserService {


  private _user?: Trainer;

  public get user(): Trainer | undefined {
    return this._user;
  }

  public set user(user: Trainer | undefined) {
    StorageUtil.storageSave<Trainer>(StorageKeys.Trainer, user!);
    this._user = user;
  }



  constructor() { 
    //storing user after login
    this._user = StorageUtil.storageRead<Trainer>(StorageKeys.Trainer);
  }

  public inFavourites(url: string): boolean {
    if (this._user) {
      let temp: boolean =  Boolean(this._user?.pokemons.find((pokemon: Pokemon) => pokemon.url === url)) //for testing
      return temp; 
    }
    return false;
  }

  public addTooFavourites(guitar: Pokemon): void {
    if(this._user) {
      this._user.pokemons.push(guitar);
    }
  }

  public removeFromFavourites(url: string): void{
    if(this._user) {
      this._user.pokemons = this._user.pokemons.filter((pokemon: Pokemon) => pokemon.url !== url);
    }
  }

  public logoutUser(): void {
    if(this._user) {
      StorageUtil.storageClear(StorageKeys.User);
      this.user = undefined;
    }
  }
}
