import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Guitar } from '../models/guitar.model';
import { Pokemon } from '../models/pokemon.model';
import { Trainer } from '../models/trainer.model';
import { User } from '../models/user.model';
import { GuitarCatalogueService } from './guitar-catalogue.service';
import { PokemonService } from './pokemon.service';
import { TrainerService } from './trainer.service';
import { UserService } from './user.service';

const {apiKey, apiTrainers} = environment
 
@Injectable({
  providedIn: 'root'
})
export class FavouriteService {


  constructor(
    private readonly pokemonService: PokemonService,
    private readonly userService: TrainerService,
    private http: HttpClient
    ) { }


  public addToFavourites(pokemonUrl: string): Observable<Trainer> {
    if (!this.userService.trainer) {
      throw new Error("addToFavourites: There is no user");
      
    }
    //save user
    const user: Trainer = this.userService.trainer;

    //get pokemon from list
    console.log(pokemonUrl);
    
    const guitar: Pokemon | undefined = this.pokemonService.findPokemon(pokemonUrl);

    //if error on get, error
    if (!guitar) {
      throw new Error("addToFavourites: No guitar with id: " + pokemonUrl); 
    }

    
    // is it in the list? remove or add
    if(this.userService.inFavourites(pokemonUrl)){
      this.userService.removeFromFavourites(pokemonUrl);
    } else {
      this.userService.addTooFavourites(guitar);
    }

    const headers = new HttpHeaders({
      'content-type': 'application/json',
      'x-api-key': apiKey
    })

    return this.http.patch<Trainer>(`${apiTrainers}/${user.id}`, {
      pokemons: [...user?.pokemons!] //Already updated
    }, {
      headers
    })
    .pipe(
      tap((updatedUser: Trainer) => {
        this.userService.trainer = updatedUser;
      })
    )
  }

}
