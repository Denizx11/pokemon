import { Injectable } from '@angular/core';
import { StorageKeys } from '../enums/storage-keys.enum';
import { Pokemon } from '../models/pokemon.model';
import { Trainer } from '../models/trainer.model';
import { StorageUtil } from '../utils/storage.util';

@Injectable({
  providedIn: 'root'
})
export class TrainerService {

  private _trainer?: Trainer;

  public get trainer(): Trainer | undefined {
    return this._trainer;
  }

  public set trainer(trainer: Trainer | undefined) {
    StorageUtil.storageSave<Trainer>(StorageKeys.Trainer, trainer!);
    this._trainer = trainer;
  }



  constructor() { 
    //storing user after login
    this._trainer = StorageUtil.storageRead<Trainer>(StorageKeys.Trainer);
    
  }

  public inFavourites(url: string): boolean {
    if (this._trainer) {
      let temp: boolean =  Boolean(this._trainer?.pokemons?.find((pokemon: Pokemon) => pokemon.url === url)) //for testing
      return temp; 
    }
    return false;
  }

  public addTooFavourites(pokemon: Pokemon): void {
    if(this._trainer) {
      this._trainer.pokemons?.push(pokemon);
    }
  }

  public removeFromFavourites(url: string): void{
    if(this._trainer) {
      this._trainer.pokemons = this._trainer.pokemons.filter((pokemon: Pokemon) => pokemon.url !== url);
    }
  }

  public logoutUser(): void {
    if(this._trainer) {
      StorageUtil.storageClear(StorageKeys.Trainer);
      this.trainer = undefined;
    }
  }
}
