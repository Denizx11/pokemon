import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Guitar } from '../models/guitar.model';
import { Pokemon } from '../models/pokemon.model';
const {apiGuitars} = environment;
@Injectable({
  providedIn: 'root'
})
export class GuitarCatalogueService {

  private _guitars: Pokemon[] = [];
  private _error: string = "";
  private _loading: boolean = false;

  public get guitars(): Pokemon[] {
    return this._guitars;
  }

  public get error(){
    return this._error;
  }

  get loading(): boolean {
    return this._loading;
  }

  constructor(private readonly http: HttpClient) { }

  public findAllGuitars(): void {

    if (this._guitars.length > 0 || this.loading){
      return;
    }

    this._loading = true;
    this.http.get<Pokemon[]>(apiGuitars)
    .pipe(
      finalize(() => {
        this._loading = false;
      })
    )
    .subscribe({
      next: (guitars: Pokemon[]) => {
        this._guitars = guitars;
      },
      error: (error: HttpErrorResponse) => {
        this._error = error.message;
      }
    })
  }

  public guitarById(url: string): Pokemon | undefined {
    return this.guitars.find((pokemon: Pokemon) => pokemon.url === url);
  }
}
