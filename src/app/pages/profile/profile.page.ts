import { Component, OnInit } from '@angular/core';
import { Guitar } from 'src/app/models/guitar.model';
import { Pokemon } from 'src/app/models/pokemon.model';
import { User } from 'src/app/models/user.model';
import { Trainer } from 'src/app/models/trainer.model';
import { UserService } from 'src/app/services/user.service';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.css']
})
export class ProfilePage implements OnInit {

  get  user(): Trainer | undefined {
    return this.userService.trainer;
  }

  get favourites(): Pokemon[] {
    if(this.userService.trainer) {
        return this.userService.trainer.pokemons;
    }
    return [];
  }

  constructor(private readonly userService: TrainerService) { }

  ngOnInit(): void {
  }

}
