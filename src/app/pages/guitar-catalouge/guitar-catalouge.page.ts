import { Component, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';

import { PokemonService } from 'src/app/services/pokemon.service';

@Component({
  selector: 'app-guitar-catalouge',
  templateUrl: './guitar-catalouge.page.html',
  styleUrls: ['./guitar-catalouge.page.css']
})
export class GuitarCatalougePage implements OnInit {

public get pokemons(): Pokemon[] {
  return this.guitarCatalogueService.pokemons;
}

public get loading(): boolean {
  return this.guitarCatalogueService.loading;
}

public get error(): string {
  return this.guitarCatalogueService.error;
}

  constructor(
    private readonly guitarCatalogueService: PokemonService
  ) { }

  ngOnInit(): void {
    this.guitarCatalogueService.findAllPokemons()
  }

}
