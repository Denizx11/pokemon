import { Component, Input, OnInit } from '@angular/core';
import { Guitar } from 'src/app/models/guitar.model';
import { Pokemon } from 'src/app/models/pokemon.model';

@Component({
  selector: 'app-guitar-list-item',
  templateUrl: './guitar-list-item.component.html',
  styleUrls: ['./guitar-list-item.component.css']
})
export class GuitarListItemComponent implements OnInit {

  @Input() pokemon?: Pokemon;

  constructor() { }

  private getIdAndImage(url?: string): any {
    const id = url?.split('/').filter( Boolean ).pop();
    return { id, image: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${ id }.png`};
  }
  imageURL:string = ""
  ngOnInit(): void {
    
    this.imageURL = this.getIdAndImage(this.pokemon?.url).image;
  }

}
