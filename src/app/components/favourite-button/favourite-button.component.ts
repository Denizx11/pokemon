import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Pokemon, PokemonResponse } from 'src/app/models/pokemon.model';
import { Trainer } from 'src/app/models/trainer.model';
import { User } from 'src/app/models/user.model';
import { FavouriteService } from 'src/app/services/favourite.service';
import { PokemonService } from 'src/app/services/pokemon.service';
import { TrainerService } from 'src/app/services/trainer.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-favourite-button',
  templateUrl: './favourite-button.component.html',
  styleUrls: ['./favourite-button.component.css'],
})
export class FavouriteButtonComponent implements OnInit {

  public loading:boolean = false;

  public isFavourite: boolean = false;
  @Input() pokemonUrl: string = '';


  constructor(
    private userService: TrainerService,
    private pokemonService:PokemonService,
    private readonly favouriteService: FavouriteService
  ) {}

  ngOnInit(): void {
    //Inputs are resolved here
    this.isFavourite = this.userService.inFavourites(this.pokemonUrl);
  }

  onFavouriteClick(): void {
    this.loading = true
    // add the pokemon to favourites
    this.favouriteService.addToFavourites(this.pokemonUrl).subscribe({
      next: (user: Trainer) => {
        this.loading = false;
        this.isFavourite = this.userService.inFavourites(this.pokemonUrl);
      },
      error: (error: HttpErrorResponse) => {
        console.log('ERROR', error.message);
      },
    });
  }
}
