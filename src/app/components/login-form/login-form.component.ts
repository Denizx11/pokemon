import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Trainer } from 'src/app/models/trainer.model';
import { User } from 'src/app/models/user.model';
import { LoginService } from 'src/app/services/login.service';
import { TrainerService } from 'src/app/services/trainer.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent {

  @Output() login: EventEmitter<void> = new EventEmitter();

  // DI
  constructor(
    private readonly loginService: LoginService,
    private readonly userService: TrainerService
    ) { }

public loginSubmit(loginForm: NgForm): void {

  // username!
  const {username} = loginForm.value;


  this.loginService.login(username)
  .subscribe({
    next: (user:Trainer) => {
      this.userService.trainer = user;
      this.login.emit();
    },
    error: () => {

    }
  })
}

}
