import { Component, OnInit } from '@angular/core';
import { Trainer } from 'src/app/models/trainer.model';
import { User } from 'src/app/models/user.model';
import { TrainerService } from 'src/app/services/trainer.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

public get user(): Trainer | undefined {
  return this.userService?.trainer;
}

constructor(private readonly userService: TrainerService) { }

ngOnInit(): void {
}


public logout(): void {
  this.userService.logoutUser();
}

}
