import { Component, OnInit } from '@angular/core';
import { GuitarCatalogueService } from './services/guitar-catalogue.service';
import { PokemonService } from './services/pokemon.service';
import { TrainerService } from './services/trainer.service';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor (
    private readonly userService: TrainerService,
    private readonly guitarService: PokemonService
  )
  {

  }

  ngOnInit(): void {
    if (this.userService.trainer)
    {
      this.guitarService.findAllPokemons();
    }
  }
}
