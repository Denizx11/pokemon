
export interface Pokemon {
    id: string,
    name: string,
    url: string
    
}
export interface PokemonResponse {
    results: Pokemon[]
}