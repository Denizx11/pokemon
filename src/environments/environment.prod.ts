export const environment = {
  production: true,
  apiUsers: "https://deniz-noroff-api.herokuapp.com/users",
  apiGuitars: "https://deniz-noroff-api.herokuapp.com/guitars"
};
